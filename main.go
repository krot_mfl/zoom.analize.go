package main

import (
    "fmt"
    "os"
    "os/exec"
    "io/ioutil"
    "encoding/json"
)

func check(e error) {
    if e != nil {
        panic(e)
    }
}

type Result struct {
    ZoomDataFormat int `json:"zoomDataFormat"`
    Status string `json:"status"`
    Result [][]int `json:"result"`
}


func main() {

    if len(os.Args) < 3 {
        fmt.Fprintf(os.Stderr, "usage: zoom.analize.go.exe input_json_file krot_exe_path\n")
        return
    }

    json_input := os.Args[1]
    krot_exe := os.Args[2]
    json_output := fmt.Sprintf("%s.out", json_input)

    dat, err := ioutil.ReadFile(json_input)
    check(err)

    var f interface{}
    err = json.Unmarshal(dat, &f)
    m := f.(map[string]interface{})
    zoom_data_format := int(m["zoomDataFormat"].(float64))

    if zoom_data_format != 1 {
        fmt.Fprintf(os.Stderr, "zoomDataFormat must be 1 (%d)\n", zoom_data_format)
        return
    }

    data_row := m["dataRow"].([]interface{})
    data_col := data_row[0].([]interface{})

    size_x := len(data_row)
    size_y := len(data_col)
    y := size_y / 4
    x := size_x / 4

    data := [][]int { 
        []int{0,                   0, x, y, 40, 1},
        []int{size_x - x,          0, x, y, 50, 1},
        []int{0,          size_y - y, x, y, 60, 1},
        []int{size_x - x, size_y - y, x, y, 70, 1}}

/*
    dataType := m["dataType"].(string)
    magnetID := m["magnetID"].(string)
    isInside := int(m["isInside"].(float64))
    sizeX := int(m["sizeX"].(float64))
    sizeY := int(m["sizeY"].(float64))
    operatorID := m["operatorID"].(string)
    recordID := m["recordID"].(string)
    recordPosition := int(m["recordPosition"].(float64))

    fmt.Println("dataType:", dataType)
    fmt.Println("magnetID:", magnetID)
    fmt.Println("isInside:", isInside)
    fmt.Println("sizeX:", sizeX)
    fmt.Println("sizeY:", sizeY)
    fmt.Println("operatorID:", operatorID)
    fmt.Println("recordID:", recordID)
    fmt.Println("recordPosition:", recordPosition)

    fmt.Printf("dataRow length: %d\n", len(data_row))
    fmt.Printf("dataRow width: %d\n", len(data_col))
*/

    dat, err = json.Marshal(Result{ZoomDataFormat: 1, Status: "ok", Result: data})
    err = ioutil.WriteFile(json_output, dat, 0644)
    check(err)

    err = exec.Command(krot_exe, fmt.Sprintf("-z%s!%s", json_input, json_output)).Start()
}
